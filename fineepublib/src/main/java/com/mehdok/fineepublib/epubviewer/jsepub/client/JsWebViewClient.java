package com.mehdok.fineepublib.epubviewer.jsepub.client;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by mehdok on 9/11/2016.
 */

public class JsWebViewClient extends WebViewClient {
    private WebViewClientListener webViewClientListener;

    public JsWebViewClient(WebViewClientListener webViewClientListener) {
        this.webViewClientListener = webViewClientListener;
    }

    @SuppressWarnings("deprecation")
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        /*if (url.contains("localhost")) {
            return onRequest(url);
        } else {
            return null;//new WebResourceResponse(url, url, null);
        }*/
        if (webViewClientListener != null) {
            return webViewClientListener.onRequest(url);
        }

        return null;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WebResourceResponse shouldInterceptRequest(WebView view,
                                                      WebResourceRequest request) {
        if (webViewClientListener != null) {
            return webViewClientListener.onRequest(request.getUrl().toString());
        }

        return null;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (webViewClientListener != null) {
            webViewClientListener.onPageFinished();
        }
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (webViewClientListener != null) {
            webViewClientListener.onPageStarted();
        }
    }

    public void setWebViewClientListener(
            WebViewClientListener webViewClientListener) {
        this.webViewClientListener = webViewClientListener;
    }
}
