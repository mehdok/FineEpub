package com.mehdok.fineepub;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mehdok.fineepublib.epubviewer.jsepub.JSBook;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            JSBook book = new JSBook(
                    "/storage/emulated/0/Android/data/com.papyrus.mehdok.snail/files/base/books/59290207.epub");
            String cover = book.getCoverImage().toString();
            Log.e("MainActivity", "cover: " + cover);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
